#!/bin/bash
#PBS -N 111_SG3
#PBS -l select=1:interconnect=fdr:ncpus=16:mpiprocs=16:mem=16gb,walltime=01:59:00
#PBS -j oe

module purge
module load gcc/6.1.0
module load fftw/3.3.4-g481
module load openmpi/1.10.3


cd $PBS_O_WORKDIR
mpirun -n 16 ~/bin/lmp_mpi_Jun19 < GB_gen.lammps # the number should be (select * ncpus)
